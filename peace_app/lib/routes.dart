import 'package:flutter/widgets.dart';
import 'package:moon/screens/complete_profile/complete_profile_screen.dart';
import 'package:moon/screens/dashboard/dashboard_screen.dart';
import 'package:moon/screens/home/home_screen.dart';
import 'package:moon/screens/login/login_screen.dart';
import 'package:moon/screens/register/register_screen.dart';
import 'package:moon/screens/reset_password/reset_password_screen.dart';
import 'package:moon/screens/splash/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (BuildContext context) => SplashScreen(),
  Dashboard.routeName: (BuildContext context) => Dashboard(),
  HomeScreen.routeName: (BuildContext context) => HomeScreen(),
  LoginScreen.routeName: (BuildContext context) => LoginScreen(),
  ResetPasswordScreen.routeName: (BuildContext context) =>
      ResetPasswordScreen(),
  RegisterScreen.routeName: (BuildContext context) => RegisterScreen(),
  CompleteProfileScreen.routeName: (BuildContext context) =>
      CompleteProfileScreen(),
};
