import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../size_config.dart';

class CustomSuffixIcon extends StatelessWidget {
  const CustomSuffixIcon({
    Key? key,
    required this.svg_icon,
  }) : super(key: key);
  final String svg_icon;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, getProportionateScreenWidth(10),
          getProportionateScreenWidth(15), getProportionateScreenWidth(10)),
      child: SvgPicture.asset(
        svg_icon,
        height: getProportionateScreenHeight(30),
      ),
    );
  }
}
