import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:moon/constants.dart';
import 'package:moon/models/Gender.dart';

import '../size_config.dart';

class GenderCardRadio extends StatelessWidget {
  late Gender _gender;

  GenderCardRadio(this._gender);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      shape: StadiumBorder(
        side: BorderSide(
          color: kInputOutlineColor,
          width: 1,
        ),
      ),
      color: _gender.isSelected ? kInputOutlineLightColor : kBackgroundColor,
      child: Container(
        width: SizeConfig.screenWidth * 0.295,
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SvgPicture.asset(
              _gender.iconName,
              height: 36,
              color: _gender.isSelected ? kBackgroundColor : kTextColor,
            ),
            Text(
              _gender.name,
              style: TextStyle(
                color: _gender.isSelected ? kBackgroundColor : kTextColor,
              ),
            )
          ],
        ),
      ),
    );
  }
}
