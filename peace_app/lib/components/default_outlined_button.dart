import 'package:flutter/material.dart';

import '../constants.dart';
import '../size_config.dart';

class DefaultOutlinedButton extends StatelessWidget {
  const DefaultOutlinedButton(
      {Key? key,
      required this.text,
      required this.icon,
      this.color = kPrimaryColor,
      required this.press})
      : super(key: key);
  final String text;
  final String icon;
  final Color color;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: getProportionateScreenHeight(50),
      child: OutlinedButton(
          style: OutlinedButton.styleFrom(
            primary: Colors.white,
            minimumSize: Size(100, 36),
            side: BorderSide(
              width: 2,
              color: color,
            ),
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(30),
              ),
            ),
          ),
          onPressed: press,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.asset(
                icon,
                width: 50,
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 50),
                  child: Text(
                    text,
                    style: TextStyle(
                      fontSize: getProportionateScreenWidth(15),
                      color: color,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
