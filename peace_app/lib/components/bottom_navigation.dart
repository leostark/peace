import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';

import '../constants.dart';

class BottomNavigation extends StatefulWidget {
  final int index;
  const BottomNavigation({Key? key, required this.index}) : super(key: key);

  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            blurRadius: 20,
            color: Colors.black.withOpacity(.1),
          )
        ],
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          child: GNav(
            rippleColor: kPrimaryColor,
            hoverColor: kPrimaryColor,
            gap: 5,
            haptic: true, // haptic feedback
            iconSize: 24,
            curve: Curves.ease,
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            duration: Duration(milliseconds: 400),
            tabBackgroundColor: kPrimaryColor.withOpacity(0.1),
            color: kPrimaryColor,
            tabs: [
              for(final entry in kPages.entries)
                GButton(
                  backgroundColor: kPrimaryColor.withOpacity(1),
                  icon: entry.value,
                  text: entry.key,
                  iconActiveColor: Colors.white,
                  textColor: Colors.white,
                )
            ],
            selectedIndex: _selectedIndex,
            onTabChange: (index) {
              setState(() {
                _selectedIndex = index;
              });
            },
          ),
        ),
      ),
    );
  }
}
