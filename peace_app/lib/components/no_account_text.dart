import 'package:flutter/material.dart';
import 'package:moon/screens/register/register_screen.dart';

import '../constants.dart';
import '../size_config.dart';

class NoAccountText extends StatelessWidget {
  const NoAccountText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Bạn chưa đăng kí tài khoản?",
          style: TextStyle(
              fontSize: getProportionateScreenWidth(14), color: kTextColor),
        ),
        SizedBox(
          width: getProportionateScreenWidth(10),
        ),
        GestureDetector(
          onTap: () => Navigator.pushNamed(context, RegisterScreen.routeName),
          child: Text(
            "Đăng kí",
            style: TextStyle(
                fontSize: getProportionateScreenWidth(14),
                color: kTextColor,
                fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }
}
