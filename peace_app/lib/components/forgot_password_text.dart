import 'package:flutter/material.dart';
import 'package:moon/screens/reset_password/reset_password_screen.dart';

import '../constants.dart';
import '../size_config.dart';

class ForgotPasswordText extends StatelessWidget {
  const ForgotPasswordText({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Bạn quên mật khẩu?",
          style: TextStyle(
              fontSize: getProportionateScreenWidth(14), color: kTextColor),
        ),
        SizedBox(
          width: getProportionateScreenWidth(10),
        ),
        GestureDetector(
          onTap: () =>
              Navigator.pushNamed(context, ResetPasswordScreen.routeName),
          child: Text(
            "Tạo mật khẩu mới",
            style: TextStyle(
                fontSize: getProportionateScreenWidth(14),
                color: kTextColor,
                fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }
}
