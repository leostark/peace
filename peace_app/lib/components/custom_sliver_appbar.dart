import 'package:flutter/material.dart';

import '../constants.dart';
import 'custom_animated_icon.dart';

class CustomSliverAppBar extends StatelessWidget {
  final String title;
  const CustomSliverAppBar({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      expandedHeight: 150,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(title,
            style: TextStyle(
              color: kTextColor,
              fontWeight: FontWeight.normal,
            )),
      ),
      actions: <Widget>[
        IconButton(
          icon: CustomAnimatedIcon(iconData: AnimatedIcons.menu_close),
          tooltip: "Menu",
          iconSize: 36,
          color: kInputOutlineColor,
          onPressed: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
        ),
      ],
    );
  }
}
