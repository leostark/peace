import 'package:flutter/material.dart';

class CustomAnimatedIcon extends StatefulWidget {
  final AnimatedIconData iconData;

  const CustomAnimatedIcon({Key? key, required this.iconData})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _CustomAnimatedIconState();
}

class _CustomAnimatedIconState extends State<CustomAnimatedIcon>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (_animationController.isCompleted) {
          _animationController.reverse();
        } else {
          _animationController.forward();
        }
      },
      child: AnimatedIcon(
        progress: _animationController,
        icon: widget.iconData,
      ),
    );
  }
}
