import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:moon/constants.dart';

import '../size_config.dart';

class SocicalButton extends StatelessWidget {
  const SocicalButton({
    Key? key,
    required this.icon_url,
    required this.onPress,
  }) : super(key: key);

  final String icon_url;
  final GestureTapCallback onPress;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        margin:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(10)),
        padding: EdgeInsets.all(getProportionateScreenWidth(10)),
        height: getProportionateScreenHeight(50),
        width: getProportionateScreenWidth(50),
        decoration:
            BoxDecoration(color: kSecondaryLightColor, shape: BoxShape.circle),
        child: SvgPicture.asset(icon_url),
      ),
    );
  }
}
