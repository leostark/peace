import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

import 'constants.dart';

ThemeData theme() {
  return ThemeData(
    appBarTheme: appBarTheme(),
    primaryColor: kPrimaryColor,
    scaffoldBackgroundColor: kBackgroundColor,
    textTheme: textTheme(),
    inputDecorationTheme: inputDecorationTheme(),
  );
}

InputDecorationTheme inputDecorationTheme() {
  return InputDecorationTheme(
    labelStyle: TextStyle(color: kTextColor),
    // floatingLabelBehavior: FloatingLabelBehavior.always,
    contentPadding: EdgeInsets.symmetric(horizontal: 35, vertical: 20),
    enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(30),
        borderSide: BorderSide(color: kInputOutlineLightColor),
        gapPadding: 8),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(30),
      borderSide: BorderSide(color: kInputOutlineColor, width: 2),
      gapPadding: 8,
    ),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(28),
      borderSide: BorderSide(color: kTextColor),
      gapPadding: 10,
    ),
    errorStyle: TextStyle(
      fontSize: 14.0,
    ),
  );
}

TextTheme textTheme() {
  return TextTheme(
      bodyText1: GoogleFonts.roboto(fontStyle: FontStyle.normal, color: kTextColor),
      bodyText2: GoogleFonts.roboto(fontStyle: FontStyle.normal, color: kTextColor));
}

AppBarTheme appBarTheme() {
  return AppBarTheme(
      color: kBackgroundColor,
      // elevation: 0,
      brightness: Brightness.light,
      iconTheme: IconThemeData(color: kInputOutlineColor),
      textTheme: TextTheme(
          headline6: TextStyle(
              color: kTextColor, fontSize: 18, fontWeight: FontWeight.normal)));
}
