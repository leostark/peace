import 'package:flutter/material.dart';

// ignore: public_member_api_docs
const Map<String, IconData> kPages = <String, IconData>{
  'Trang chủ': Icons.home,
  "Tra cứu": Icons.search,
  "Tin nhắn": Icons.question_answer,
  "Cá nhân": Icons.person,
};

const Color kPrimaryColor = Color(0xFF2698D4);
const Color kPrimaryLightColor = Color(0xFF6AC9FF);
const Color kPrimaryDartColor = Color(0xFF006AA2);
const Color kTextColor = Color(0xFF000A12);
const Color kInputOutlineColor = Color(0xFF000A12);
const Color kInputOutlineLightColor = Color(0xFF263238);
const Color kZaloColor = Color(0xFF0068FF);

const Color kBackgroundColor = Color(0xFFCEDFE7);

const Color kSecondaryLightColor = Color(0xFFECEFF1);

const Duration kAnimationDuration = Duration(milliseconds: 200);

const Map<String, AnimatedIconData> kAnimatedIcons = <String, AnimatedIconData>{
  'menu_close': AnimatedIcons.menu_close,
};

final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
final RegExp passwordValidatorRegExp =
    RegExp(r"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$");

final RegExp firstNameValidatorRegExp = RegExp(r"^(([A-Z][a-z]+) ?){1,}$");
final RegExp lastNameValidatorRegExp = RegExp(r"^(([A-Z][a-z]+) ?){1,}$");
final RegExp phoneNumberValidatorRegExp =
    RegExp(r"^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$");

const String kEmailNullError = "Vui lòng nhập email";
const String kInvalidEmailError = "Vui lòng nhập đúng email";
const String kPasswordNullError = "Vui lòng nhập mật khẩu";
const String kPasswordTooShortError = "Mật khẩu phải nhiều hơn 8 kí tự";
const String kMatchPasswordError = "Mật khẩu chưa đạt tiêu chuẩn";
const String kPasswordValidatorRegExpError =
    "Password length from 8 to 32 digits \nwith uppercase, lowercase, number \nand special characters (!@#\$&*~)";

const String kFirstNameNullError = "Please enter your first name";
const String kLastNameNullError = "Please enter your last name";
const String kPhoneNumberNullError = "Vui lòng nhập số điện thoại";
const String kDateOfBirthNullError = "Vui lòng chọn ngày sinh";
