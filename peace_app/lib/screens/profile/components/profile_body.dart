import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:moon/constants.dart';

class ProfileBody extends StatefulWidget {
  const ProfileBody({Key? key}) : super(key: key);

  @override
  _ProfileBodyState createState() => _ProfileBodyState();
}

class _ProfileBodyState extends State<ProfileBody> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    Map<String, int> infoStatistical = {
      "Bài đăng": 17,
      "Theo dõi": 06,
      "Đang theo dõi": 04,
      "Bạn bè": 21
    };
    Map<String, IconData> actionCards = {
      'Trang cá nhân': Icons.description_outlined,
      'Thông tin QR': Icons.qr_code_2_outlined,
      'Hoạt động': Icons.restore_outlined,
      '3': Icons.qr_code_2_outlined,
      '4': Icons.qr_code_2_outlined,
    };
    List<Map<String, String>> redirectCards = [
      {
        "title": 'Nhật ký y tế',
        'icon': 'assets/images/needle.png',
        'routeName': ''
      },
      {
        'title': 'Khai báo y tế',
        'icon': 'assets/images/identify.png',
        'routeName': ''
      },
      {
        'title': 'Tình trạng sức khoẻ',
        'icon': 'assets/images/medical_heart.png',
        'routeName': ''
      },
      {
        'title': 'Nhật kí hành trình',
        'icon': 'assets/images/location_log.png',
        'routeName': ''
      },
      {
        'title': 'Hỗ trợ',
        'icon': 'assets/images/medical_bag.png',
        'routeName': ''
      },
    ];

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            height: size.height * 0.5,
            decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                image: AssetImage('assets/images/avt.jpg'),
                fit: BoxFit.cover,
              ),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(36),
                bottomRight: Radius.circular(36),
              ),
              boxShadow: [
                BoxShadow(
                  color: Color(0x363535).withOpacity(0.16),
                  spreadRadius: 5,
                  blurRadius: 26,
                  offset: Offset(0, 20),
                )
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                userInformationCard(infoStatistical),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: size.height * 0.02),
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: size.width * 0.25,
                  child: ListView.builder(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    shrinkWrap: true,
                    itemCount: actionCards.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.symmetric(horizontal: 5),
                        child: InkWell(
                          onTap: () {},
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: BackdropFilter(
                              filter: ImageFilter.blur(sigmaY: 10, sigmaX: 10),
                              child: Container(
                                width: size.width * 0.25,
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  color: kPrimaryColor,
                                  border: Border.all(
                                    width: 1,
                                    color: Colors.white.withOpacity(0.6),
                                  ),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  children: <Widget>[
                                    Icon(
                                      actionCards.values.toList()[index],
                                      size: 36,
                                      color: Colors.white,
                                    ),
                                    Text(
                                      actionCards.keys.toList()[index],
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12.5),
                                      textAlign: TextAlign.center,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: redirectCards.length,
                    shrinkWrap: true,
                    padding:
                        EdgeInsets.symmetric(vertical: size.height * 0.02 - 5),
                    itemBuilder: (context, index) {
                      return profileRedirectCard(size, redirectCards, index);
                    },
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  InkWell profileRedirectCard(
      Size size, List<Map<String, String>> redirectCards, int index) {
    return InkWell(
      onTap: () {},
      child: Container(
        margin: EdgeInsets.all(5),
        height: size.width * 0.2,
        width: size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          boxShadow: [
            BoxShadow(
              color: Color(0x363535).withOpacity(0.16),
              spreadRadius: 5,
              blurRadius: 26,
              offset: Offset(0, 20),
            )
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: BackdropFilter(
            filter: ImageFilter.blur(
              sigmaX: 20,
              sigmaY: 20,
            ),
            child: Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.5),
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  width: 1,
                  color: Colors.white.withOpacity(0.5),
                ),
              ),
              child: Row(
                children: <Widget>[
                  Image.asset(redirectCards[index]['icon'].toString()),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Text(
                        redirectCards[index]['title'].toString(),
                        style: TextStyle(
                          color: kTextColor,
                          fontWeight: FontWeight.w700,
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ),
                  Icon(
                    Icons.arrow_forward_ios_outlined,
                    color: kTextColor,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Container userInformationCard(Map<String, int> infoStatistical) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(36),
        boxShadow: [
          BoxShadow(
            color: Color(0x353636).withOpacity(0.2),
            spreadRadius: 10,
            blurRadius: 10,
          )
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(36),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaY: 10, sigmaX: 10),
          child: Container(
            padding: EdgeInsets.only(top: 5),
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.2),
              borderRadius: BorderRadius.circular(36),
              border: Border.all(
                width: 1,
                color: Colors.white.withOpacity(0.2),
              ),
            ),
            child: Column(
              children: <Widget>[
                Container(
                  child: Column(
                    children: [
                      Text(
                        "Đỗ Tấn Tĩnh",
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.white,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          primary: Colors.white,
                          minimumSize: Size(100, 36),
                          side: BorderSide(
                            color: Colors.white,
                          ),
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(25),
                            ),
                          ),
                        ),
                        child: Text("Xem hồ sơ"),
                        onPressed: () {},
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: new List.generate(
                      infoStatistical.length,
                      (index) => statisticItem(infoStatistical, index),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Positioned buildStatistical(Map<String, int> infoStatistical, Size size) {
    return Positioned(
      right: 0,
      left: 0,
      bottom: 0,
      // height: size.height * 0.1,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: new List.generate(
            infoStatistical.length,
            (index) => statisticItem(infoStatistical, index),
          ),
        ),
      ),
    );
  }

  Positioned buildQR(Size size) {
    return Positioned(
      bottom: size.height * 0.1,
      right: size.width * 0.05,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          primary: Colors.white,
          padding: EdgeInsets.all(10),
          side: BorderSide(
            color: Colors.white.withOpacity(0),
          ),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(40),
            ),
          ),
        ),
        child: Icon(
          Icons.qr_code_2,
          size: 45,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
    );
  }

  Positioned buildMenu(Size size) {
    return Positioned(
      top: size.width * 0.1,
      right: size.width * 0.05,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
          child: OutlinedButton(
            style: OutlinedButton.styleFrom(
              primary: Colors.white,
              padding: EdgeInsets.all(10),
              side: BorderSide(
                color: Colors.white.withOpacity(0),
              ),
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(15),
                ),
              ),
            ),
            child: Icon(
              Icons.menu_open,
              size: 36,
              color: Colors.white,
            ),
            onPressed: () {},
          ),
        ),
      ),
    );
  }

  Expanded statisticItem(Map<String, int> infoStatistical, int index) {
    return Expanded(
      child: Column(
        children: [
          Text(
            infoStatistical.values.toList()[index].toString(),
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
          Text(
            infoStatistical.keys.toList()[index],
            style: TextStyle(
              color: Colors.white,
              fontSize: 12.8,
              fontWeight: FontWeight.normal,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
