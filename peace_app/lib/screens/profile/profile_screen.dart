import 'package:flutter/material.dart';

import 'components/profile_body.dart';
class ProfileScreen extends StatefulWidget {
  static String routeName = "/profile";
  static String titlePage = "Hồ sơ";
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ProfileBody(),
    );
  }
}
