import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:moon/components/custom_suffix_icon.dart';
import 'package:moon/components/default_button.dart';
import 'package:moon/screens/dashboard/dashboard_screen.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  late String email;
  late String password;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          buildCellphoneFormField(),
          SizedBox(
            height: getProportionateScreenHeight(25),
          ),
          buildPasswordFormField(),
          SizedBox(
            height: getProportionateScreenHeight(25),
          ),
          DefaultButton(
            icon: 'assets/images/peace_logo_opposite_transparent.png',
            text: "Đăng nhập",
            press: () {
              if (_formKey.currentState!.validate()) {
                // _formKey.currentState!.save();
                Navigator.pushNamed(context, Dashboard.routeName);
              }
            },
          )
        ],
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      keyboardType: TextInputType.visiblePassword,
      obscureText: true,
      validator: (value) {
        if (value!.isEmpty) {
          return kPasswordNullError;
        }
      },
      decoration: InputDecoration(
          labelText: "Mật khẩu",
          hintText: "Nhập mật khẩu",
          // floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon:
              CustomSuffixIcon(svg_icon: "assets/icons/lock_filled.svg")),
    );
  }

  TextFormField buildCellphoneFormField() {
    return TextFormField(
      onSaved: (newValue) => email = newValue!,
      keyboardType: TextInputType.emailAddress,
      validator: (value) {
        if (value!.isEmpty) {
          return kPhoneNumberNullError;
        }
      },
      decoration: InputDecoration(
        labelText: "Số điện thoại",
        hintText: "Nhập số điện thoại",
        suffixIcon: CustomSuffixIcon(svg_icon: "assets/icons/phone_filled.svg"),
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('password', password));
  }
}
