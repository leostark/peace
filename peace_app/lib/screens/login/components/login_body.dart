import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:moon/components/default_outlined_button.dart';
import 'package:moon/components/forgot_password_text.dart';
import 'package:moon/components/no_account_text.dart';
import 'package:moon/size_config.dart';

import '../../../constants.dart';
import 'login_form.dart';

class LoginBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoginBodyState();
}

class LoginBodyState extends State<LoginBody> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: SizeConfig.screenHeight,
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.screenWidth * 0.05,
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/background_2.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: BackdropFilter(
              filter: ImageFilter.blur(
                sigmaX: 10,
                sigmaY: 10,
              ),
              child: Container(
                padding: EdgeInsets.symmetric(
                  vertical: SizeConfig.screenHeight * 0.05,
                  horizontal: SizeConfig.screenWidth * 0.05,
                ),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.3),
                  border: Border.all(
                    width: 2,
                    color: Colors.white.withOpacity(0.1),
                  ),
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "Chào mừng đến với Peace",
                      style: TextStyle(
                          color: kPrimaryColor,
                          fontSize: getProportionateScreenWidth(20),
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      "Đăng nhập bằng tài khoản của bạn",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: kTextColor, fontSize: 16),
                    ),
                    SizedBox(
                      height: SizeConfig.screenHeight * 0.04,
                    ),
                    LoginForm(),
                    SizedBox(
                      height: SizeConfig.screenHeight * 0.02,
                    ),
                    Text(
                      "Hoặc",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: kTextColor,
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.screenHeight * 0.02,
                    ),
                    DefaultOutlinedButton(
                      color: kZaloColor,
                      icon: 'assets/images/zalo.png',
                      text: "Tiếp tục bằng Zalo",
                      press: () {},
                    ),
                    SizedBox(
                      height: SizeConfig.screenHeight * 0.04,
                    ),
                    ForgotPasswordText(),
                    SizedBox(
                      height: SizeConfig.screenHeight * 0.02,
                    ),
                    NoAccountText(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
