import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:moon/screens/home/home_screen.dart';
import 'package:moon/screens/investigation/investigation_screen.dart';
import 'package:moon/screens/message/message_screen.dart';
import 'package:moon/screens/profile/profile_screen.dart';

import '../../constants.dart';
class Dashboard extends StatefulWidget {
  static String routeName = '/';
  const Dashboard({Key? key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  static int currentPage = 0;
  List<Widget> pages = <Widget>[
    HomeScreen(),
    InvestigationScreen(),
    MessageScreen(),
    ProfileScreen()
  ];

  _changePage(int index){
    setState(() {
      currentPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages[currentPage],
      bottomNavigationBar: buildBottomNavigation(),
    );
  }

  Container buildBottomNavigation() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            blurRadius: 20,
            color: Colors.black.withOpacity(.1),
          )
        ],
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          child: GNav(
            // rippleColor: kPrimaryColor,
            hoverColor: kPrimaryColor,
            gap: 5,
            haptic: true,
            iconSize: 24,
            curve: Curves.ease,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
            duration: Duration(milliseconds: 400),
            tabBackgroundColor: kPrimaryColor.withOpacity(0.1),
            color: kPrimaryColor,
            tabs: [
              for(final entry in kPages.entries)
                GButton(
                  backgroundColor: kPrimaryColor.withOpacity(1),
                  icon: entry.value,
                  text: entry.key,
                  iconActiveColor: Colors.white,
                  textColor: Colors.white,
                )
            ],
            selectedIndex: currentPage,
            onTabChange: (index) {
              _changePage(index);
            },
          ),
        ),
      ),
    );
  }
}



