import 'package:flutter/material.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class SplashContent extends StatelessWidget {
  const SplashContent({
    Key? key,
    this.text = "Welcome to moon application",
    this.image_url = "assets/images/splash_1.png",
  }) : super(key: key);
  final String text, image_url;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Spacer(),
        Text(
          "MOON",
          style: TextStyle(
              fontSize: getProportionateScreenWidth(36),
              color: kPrimaryColor,
              fontWeight: FontWeight.bold),
        ),
        Text(
          text,
          textAlign: TextAlign.center,
        ),
        Spacer(),
        Image.asset(
          image_url,
          height: getProportionateScreenHeight(250),
          width: getProportionateScreenWidth(250),
        )
      ],
    );
  }
}
