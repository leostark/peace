import 'package:flutter/material.dart';
import 'package:moon/screens/splash/components/splash_body.dart';
import 'package:moon/size_config.dart';

class SplashScreen extends StatelessWidget {
  static String routeName = "/splash";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SplashBody(),
    );
  }
}
