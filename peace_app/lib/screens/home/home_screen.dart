import 'package:flutter/material.dart';

import 'components/home_body.dart';

class HomeScreen extends StatefulWidget{
  static String routeName = '/home';
  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: HomeBody(),
    );
  }
}