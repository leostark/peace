import 'package:flutter/material.dart';
import 'package:moon/components/custom_sliver_appbar.dart';
import 'package:moon/components/custom_suffix_icon.dart';
import 'package:moon/components/default_button.dart';
import 'package:moon/components/no_account_text.dart';
import 'package:moon/constants.dart';
import 'package:moon/size_config.dart';

class ResetPasswordBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ResetPasswordBodyState();
}

class _ResetPasswordBodyState extends State<ResetPasswordBody> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        CustomSliverAppBar(
          title: "Reset password",
        ),
        SliverToBoxAdapter(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(10)),
            child: Column(
              children: <Widget>[
                SizedBox(height: SizeConfig.screenHeight * 0.02),
                Text(
                  "Please enter your email \nand we will send you a link to return to your account",
                  style: TextStyle(color: kTextColor, height: 1.5),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.1),
                ResetPasswordForm(),
                SizedBox(height: SizeConfig.screenHeight * 0.1),
                NoAccountText(),
                SizedBox(height: SizeConfig.screenHeight * 0.1),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class ResetPasswordForm extends StatefulWidget {
  @override
  _ResetPasswordFormState createState() => _ResetPasswordFormState();
}

class _ResetPasswordFormState extends State<ResetPasswordForm> {
  final _formKey = GlobalKey<FormState>();
  late String email;
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            onSaved: (newValue) => email = newValue!,
            keyboardType: TextInputType.emailAddress,
            validator: (value) {
              if (value!.isEmpty) {
                return kEmailNullError;
              } else if (!emailValidatorRegExp.hasMatch(value)) {
                return kInvalidEmailError;
              }
            },
            decoration: InputDecoration(
                labelText: "Email",
                hintText: "Enter your email",
                // floatingLabelBehavior: FloatingLabelBehavior.always,
                suffixIcon:
                    CustomSuffixIcon(svg_icon: "assets/icons/email.svg")),
          ),
          SizedBox(height: SizeConfig.screenHeight * 0.1),
          DefaultButton(
            icon: 'assets/images/peace_logo_opposite_transparent.png',
            text: "Continue",
            press: () {
              if (_formKey.currentState!.validate()) {
                // send email
              }
            },
          ),
        ],
      ),
    );
  }
}
