import 'package:flutter/material.dart';
import 'package:moon/screens/reset_password/components/reset_password_body.dart';

class ResetPasswordScreen extends StatelessWidget {
  static String routeName = "/reset_password";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ResetPasswordBody(),
    );
  }
}
