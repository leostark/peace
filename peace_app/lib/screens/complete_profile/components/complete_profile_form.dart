import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:moon/components/custom_suffix_icon.dart';
import 'package:moon/components/default_button.dart';
import 'package:moon/components/gender_card_radio.dart';
import 'package:moon/constants.dart';
import 'package:moon/models/Gender.dart';
import 'package:moon/screens/login/login_screen.dart';

import '../../../size_config.dart';

class CompleteProfileForm extends StatefulWidget {
  @override
  _CompleteProfileState createState() => _CompleteProfileState();
}

class _CompleteProfileState extends State<CompleteProfileForm> {
  final _formKey = GlobalKey<FormState>();
  late String cellphone;
  final textEditingController = TextEditingController();
  List<Gender> genders = <Gender>[];

  @override
  void initState() {
    super.initState();
    genders.add(new Gender("Male", "assets/icons/male.svg", false));
    genders.add(new Gender("Female", "assets/icons/female.svg", false));
    genders.add(new Gender("Others", "assets/icons/transgender.svg", false));
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          children: [
            buildFirstNameFormField(),
            SizedBox(
              height: getProportionateScreenHeight(25),
            ),
            buildLastNameFormField(),
            SizedBox(
              height: getProportionateScreenHeight(25),
            ),
            buildGenderRadio(),
            SizedBox(
              height: getProportionateScreenHeight(25),
            ),
            buildDateOfBirthFormField(),
            SizedBox(
              height: getProportionateScreenHeight(25),
            ),
            buildCellphoneFormField(),
            SizedBox(
              height: getProportionateScreenHeight(30),
            ),
            DefaultButton(
              icon: 'assets/images/peace_logo_opposite_transparent.png',
              text: "Continue",
              press: () {
                if (_formKey.currentState!.validate()) {
                  Navigator.pushNamed(context, LoginScreen.routeName);
                }
              },
            ),
            SizedBox(
              height: getProportionateScreenHeight(30),
            ),
            Text(
              "By continuing your confirm that your agree \nwith our Term and Condition",
              textAlign: TextAlign.center,
              style: TextStyle(color: kTextColor, height: 1.5),
            )
          ],
        ),
      ),
    );
  }

  Container buildGenderRadio() {
    return Container(
      width: double.infinity,
      height: 65,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ListView.builder(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemCount: genders.length,
            padding: EdgeInsets.zero,
            itemBuilder: (context, index) {
              return InkWell(
                child: GenderCardRadio(genders[index]),
                onTap: () {
                  setState(
                    () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      genders.forEach((gender) => gender.isSelected = false);
                      genders[index].isSelected = true;
                    },
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }

  TextFormField buildCellphoneFormField() {
    return TextFormField(
      onSaved: (newValue) => cellphone = newValue!,
      keyboardType: TextInputType.phone,
      validator: (value) {
        if (value!.isEmpty) {
          return kPhoneNumberNullError;
        }
      },
      decoration: InputDecoration(
          labelText: "Phone number",
          hintText: "Enter your phone number",
          suffixIcon: CustomSuffixIcon(svg_icon: "assets/icons/phone.svg")),
    );
  }

  TextFormField buildFirstNameFormField() {
    return TextFormField(
      onSaved: (newValue) => cellphone = newValue!,
      keyboardType: TextInputType.name,
      validator: (value) {
        if (value!.isEmpty) {
          return kFirstNameNullError;
        }
      },
      decoration: InputDecoration(
          labelText: "First Name",
          hintText: "Enter your first name",
          suffixIcon:
              CustomSuffixIcon(svg_icon: "assets/icons/person_outline.svg")),
    );
  }

  TextFormField buildLastNameFormField() {
    return TextFormField(
      onSaved: (newValue) => cellphone = newValue!,
      keyboardType: TextInputType.name,
      validator: (value) {
        if (value!.isEmpty) {
          return kLastNameNullError;
        }
      },
      decoration: InputDecoration(
          labelText: "Last Name",
          hintText: "Enter your last name",
          suffixIcon:
              CustomSuffixIcon(svg_icon: "assets/icons/person_outline.svg")),
    );
  }

  TextFormField buildDateOfBirthFormField() {
    return TextFormField(
      onSaved: (newValue) => cellphone = newValue!,
      keyboardType: TextInputType.datetime,
      readOnly: true,
      controller: textEditingController,
      validator: (value) {
        if (value!.isEmpty) {
          return kDateOfBirthNullError;
        }
      },
      decoration: InputDecoration(
          labelText: "Date of birth",
          hintText: "Enter your date of birth",
          suffixIcon:
              CustomSuffixIcon(svg_icon: "assets/icons/date_of_birth.svg")),
      onTap: () {
        DatePicker.showDatePicker(context,
            showTitleActions: true,
            minTime: DateTime(1900, 01, 01),
            maxTime: DateTime.now(),
            theme: DatePickerTheme(
                headerColor: kBackgroundColor,
                backgroundColor: kBackgroundColor,
                itemStyle: TextStyle(
                    color: kTextColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
                doneStyle: TextStyle(color: kPrimaryColor, fontSize: 16),
                cancelStyle: TextStyle(color: kTextColor, fontSize: 16)),
            onConfirm: (date) {
          textEditingController.text = DateFormat.yMMMMd().format(date);
        }, currentTime: DateTime(2000, 06, 15), locale: LocaleType.en);
      },
    );
  }
}
