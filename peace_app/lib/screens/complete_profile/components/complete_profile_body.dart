import 'package:flutter/material.dart';
import 'package:moon/components/custom_sliver_appbar.dart';
import 'package:moon/constants.dart';
import 'package:moon/screens/complete_profile/components/complete_profile_form.dart';
import 'package:moon/size_config.dart';

class CompleteProfileBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CompleteProfileBodyState();
}

class _CompleteProfileBodyState extends State<CompleteProfileBody> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        CustomSliverAppBar(title: "Complete Profile"),
        SliverToBoxAdapter(
          child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(10)),
              child: Column(
                children: [
                  Text(
                    "Complete Profile",
                    style: TextStyle(
                      color: kTextColor,
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.screenHeight*0.06,
                  ),
                  CompleteProfileForm(),
                ],
              )),
        )
      ],
    );
  }
}
