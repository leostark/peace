
import 'package:flutter/material.dart';
import 'package:moon/screens/investigation/components/investigation_body.dart';
class InvestigationScreen extends StatefulWidget {
  const InvestigationScreen({Key? key}) : super(key: key);

  @override
  _InvestigationScreenState createState() => _InvestigationScreenState();
}

class _InvestigationScreenState extends State<InvestigationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InvestigationBody(),
    );
  }
}
