import 'package:flutter/material.dart';
import 'package:moon/components/custom_sliver_appbar.dart';
import 'package:moon/components/social_button.dart';
import 'package:moon/constants.dart';
import 'package:moon/screens/register/components/register_form.dart';
import 'package:moon/size_config.dart';

class RegisterBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RegisterBodyState();
}

class _RegisterBodyState extends State<RegisterBody> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        CustomSliverAppBar(title: "Register"),
        SliverToBoxAdapter(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(10)),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: SizeConfig.screenHeight * 0.02,
                ),
                Text(
                  "Complete your details \nor continue with your social media account",
                  style: TextStyle(color: kTextColor,
                    height: 1.5
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: SizeConfig.screenHeight * 0.05,
                ),
                RegisterForm(),
                SizedBox(
                  height: SizeConfig.screenHeight * 0.08,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SocicalButton(
                      icon_url: "assets/icons/google.svg",
                      onPress: () {},
                    ),
                    SocicalButton(
                      icon_url: "assets/icons/facebook.svg",
                      onPress: () {},
                    ),
                    SocicalButton(
                      icon_url: "assets/icons/twitter.svg",
                      onPress: () {},
                    )
                  ],
                ),
                SizedBox(
                  height: getProportionateScreenHeight(25),
                ),
                Text(
                  "By continuing your confirm \nthat you agree with our Term and Condition",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: kTextColor,
                    height: 1.5
                  ),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(25),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
