const statisticRoute = require('./statistic');
const siteRoute = require('./site');

function route(app) {
    app.use('/statistic', statisticRoute);
    app.use('/', siteRoute);
}

module.exports = route;
