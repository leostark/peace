const express = require('express');
const router = express.Router();

const statisticController = require('../app/controllers/StatisticController');

// go to home page
router.use('/:slug', statisticController.show);
router.use('/', statisticController.index);

module.exports = router;
