const path = require('path');
const express = require('express');
const morgan = require('morgan');
const handlebars = require('express-handlebars');
const app = express();
const PORT = 3000;

app.use(morgan('combined'));
// template engine
app.engine(
    'hbs',
    handlebars({
        extname: '.hbs',
    }),
);
app.set('view engine', 'hbs');

// static resources
app.set('views', path.join(__dirname, 'resources/views'));
app.use(express.static(path.join(__dirname, 'public')));

// require routes
const route = require('./routes');
route(app);

app.listen(PORT, () =>
    console.log(`Your website listening at http://localhost:${PORT}`),
);
